﻿using Otus.Teaching.PromoCodeFactory.DataAccess.Data;

namespace Otus.Teaching.PromoCodeFactory.WebHost
{
    public static class DbInitializer
    {
        public static void Initialize(DataContext context)
        {
            context.Database.EnsureCreated();

            var employee = FakeDataFactory.Employees;
            //var customer = FakeDataFactory.Customers;
            var preference = FakeDataFactory.Preferences;
            //var promo = FakeDataFactory.PromoCodes;

            //context.PromoCodes.AddRange(promo);
            context.Preferences.AddRange(preference);
            //context.Customers.AddRange(customer);
            context.Employees.AddRange(employee);

            context.SaveChanges();
        }
    }
}