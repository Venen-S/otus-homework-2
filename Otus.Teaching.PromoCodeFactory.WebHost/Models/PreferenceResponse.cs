﻿using System.Collections.Generic;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class PreferenceResponse
    {
        public string Name { get; set; }
        public string Genre { get; set; }
        public PromoCode PromoCode { get; set; }
        public IList<CustomerPreference> CustomerPreferences { get; set; }
    }
}