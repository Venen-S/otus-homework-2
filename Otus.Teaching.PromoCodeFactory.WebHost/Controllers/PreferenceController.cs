﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PreferenceController : ControllerBase
    {
        private readonly IRepository<Preference> _preferenceRepository;

        public PreferenceController(IRepository<Preference> preferenceRepository)
        {
            _preferenceRepository = preferenceRepository;
        }

        /// <summary>
        /// Весь список предпочтений
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<PreferenceResponse>> GetPreferenceAsync()
        {
            var preferences = await _preferenceRepository.GetAllAsync();
            var list = new List<PreferenceResponse>();
            foreach (var preference in preferences)
            {
                var listPreference = new List<CustomerPreference>();
                if (preference?.CustomerPreferences != null)
                    foreach (var customerPreference in preference?.CustomerPreferences)
                    {
                        listPreference.Add(customerPreference);
                    }

                list.Add(new PreferenceResponse()
                {
                    Name = preference.Name,
                    PromoCode = preference.PromoCode,
                    Genre = preference.Genre,
                    CustomerPreferences = listPreference
                });
            }

            return list;
        }
    }
}
