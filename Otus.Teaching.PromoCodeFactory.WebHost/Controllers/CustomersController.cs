﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly IRepository<Customer> _customRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public CustomersController(IRepository<Customer> customRepository,
            IRepository<Preference> preferenceRepository)
        {
            _customRepository = customRepository;
            _preferenceRepository = preferenceRepository;
        }

        /// <summary>
        /// Весь список кастомеров
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<CustomerShortResponse>> GetCustomersAsync()
        {
            var custom = await _customRepository.GetAllAsync();
            return custom.Select(c =>
                new CustomerShortResponse()
                {
                    Id = c.Id,
                    Email = c.Email,
                    FirstName = c.FirstName,
                    LastName = c.LastName
                }).ToList();
        }
        
        /// <summary>
        /// Получение кастомера по ид
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var custom = await _customRepository.GetByIdAsync(id);
            return new CustomerResponse()
            {
                Id = custom.Id,
                Email = custom.Email,
                FirstName = custom.FirstName,
                LastName = custom.LastName,
                PromoCodes = new List<PromoCodeShortResponse>()
                {
                    new PromoCodeShortResponse()
                    {
                        Id = custom.PromoCode.Id,
                        ServiceInfo = custom.PromoCode.ServiceInfo,
                        BeginDate = custom.PromoCode.BeginDate.ToString(CultureInfo.CurrentCulture),
                        Code = custom.PromoCode.Code,
                        EndDate = custom.PromoCode.EndDate.ToString(CultureInfo.CurrentCulture),
                        PartnerName = custom.PromoCode.PartnerName
                    }
                }
            };
        }
        

        /// <summary>
        /// создание кастомера
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            var id = Guid.NewGuid();
            var cust = new Customer()
            {
                Id = id,
                Email = request.Email,
                FirstName = request.FirstName,
                LastName = request.LastName
            };
            cust = await _customRepository.CreateEntity(cust);
            cust.CustomerPreferences = new List<CustomerPreference>(new CustomerPreference[]
            {
                new CustomerPreference()
                {
                    Customer = cust,
                    CustomerId = cust.Id,
                    PreferenceId = request.PreferenceIds[0],
                    Preference = await _preferenceRepository.GetByIdAsync(request.PreferenceIds[0])
                }
            });
            cust = await _customRepository.UpdateEntity(cust);
        }
        
        /// <summary>
        /// Обновление кастомера
        /// </summary>
        /// <param name="id">идентификатор</param>
        /// <param name="request">запрос</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<bool> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            var customer = await _customRepository.GetByIdAsync(id);
            if (customer is null)  
                new Exception("Кастомер не найден");

            customer.Email = request.Email;
            customer.FirstName = request.FirstName;
            customer.LastName = request.LastName;
            customer.CustomerPreferences = new List<CustomerPreference>();
            try
            {
                foreach (var guid in request.PreferenceIds)
                {
                    var preference = await _preferenceRepository.GetByIdAsync(guid);
                    if (preference is null) continue;
                    customer.CustomerPreferences.Add(new CustomerPreference()
                    {
                        Customer = customer,
                        CustomerId = customer.Id,
                        Preference = preference,
                        PreferenceId = preference.Id
                    });
                }

                await _customRepository.UpdateEntity(customer);
            }
            catch (Exception ex)
            {
                return false;
            }

            return true;
        }
        
        /// <summary>
        /// Удаление кастомера
        /// </summary>
        /// <returns></returns>
        [HttpDelete]
        public async Task<bool> DeleteCustomer(Guid id)
        {
            var custom = await _customRepository.GetByIdAsync(id);
            if (custom is null) return false;

            var flag = await _customRepository.DeleteEntity(custom);
            return flag;
        }
    }
}