﻿using System;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public static class FakeDataFactory
    {
        public static IEnumerable<Employee> Employees => new List<Employee>()
        {
            new Employee()
            {
                Id = Guid.Parse("451533d5-d8d5-4a11-9c7b-eb9f14e1a32f"),
                Email = "owner@somemail.ru",
                FirstName = "Иван",
                LastName = "Сергеев",
                Role = Roles.FirstOrDefault(x => x.Name == "Admin"),
                AppliedPromocodesCount = 5
            },
            new Employee()
            {
                Id = Guid.Parse("f766e2bf-340a-46ea-bff3-f1700b435895"),
                Email = "andreev@somemail.ru",
                FirstName = "Петр",
                LastName = "Андреев",
                Role = Roles.FirstOrDefault(x => x.Name == "PartnerManager"),
                AppliedPromocodesCount = 10
            },
        };

        public static IEnumerable<Role> Roles => new List<Role>()
        {
            new Role()
            {
                Id = Guid.Parse("53729686-a368-4eeb-8bfa-cc69b6050d02"),
                Name = "Admin",
                Description = "Администратор",
            },
            new Role()
            {
                Id = Guid.Parse("b0ae7aac-5493-45cd-ad16-87426a5e7665"),
                Name = "PartnerManager",
                Description = "Партнерский менеджер"
            }
        };
        
        public static IEnumerable<Preference> Preferences => new List<Preference>()
        {
            new Preference()
            {
                Id = Guid.Parse("ef7f299f-92d7-459f-896e-078ed53ef99c"),
                Name = "Театр",
                PromoCode = new PromoCode()
                {
                    Id = Guid.Parse("c4bda72e-fc75-4256-a956-4760b3658cbd"),
                    ServiceInfo = "service",
                    Code = "genetic code",
                    PartnerName = "random",
                    BeginDate = DateTime.Now.AddDays(-2),
                    EndDate = DateTime.Now.AddDays(2),
                    Customers = new List<Customer>()
                    {
                        new Customer()
                        {
                            Id = Guid.Parse("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"),
                            Email = "ivan_sergeev@mail.ru",
                            FirstName = "Иван",
                            LastName = "Петров"
                        }
                    }
                },
                Genre = "Щелкунчик",
                CustomerPreferences = new List<CustomerPreference>()
                {
                    new CustomerPreference()
                    {
                        CustomerId = Guid.Parse("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"),
                        PreferenceId = Guid.Parse("ef7f299f-92d7-459f-896e-078ed53ef99c")
                    }
                }

            },
            new Preference()
            {
                Id = Guid.Parse("c4bda62e-fc74-4256-a956-4760b3858cbd"),
                Name = "Семья",
                Genre = "Личная жизнь",
                PromoCode = new PromoCode()
                {
                    Id = Guid.Parse("c9bda72e-fc15-4256-a356-4260b3655cbd"),
                    ServiceInfo = "service2",
                    Code = "genetic code2",
                    PartnerName = "random2",
                    BeginDate = DateTime.Now.AddDays(-3),
                    EndDate = DateTime.Now.AddDays(3),
                    Customers = new List<Customer>()
                    {
                        new Customer()
                        {
                            Id = Guid.Parse("a6c8c6b1-4349-45b0-ab31-245340aaf0f0"),
                            Email = "ioan_sergeev@mail.ru",
                            FirstName = "Иoан",
                            LastName = "Петров",
                        }
                    }
                },
                CustomerPreferences = new List<CustomerPreference>()
                {
                    new CustomerPreference()
                    {
                        CustomerId = Guid.Parse("a6c8c6b1-4349-45b0-ab31-245340aaf0f0"),
                        PreferenceId = Guid.Parse("c4bda62e-fc74-4256-a956-4760b3858cbd")
                    }
            },}
            
        };

        //public static IEnumerable<PromoCode> PromoCodes => new List<PromoCode>()
        //{
        //    new PromoCode()
        //    {
        //        Id = Guid.Parse("c4bda72e-fc75-4256-a956-4760b3658cbd"),
        //        ServiceInfo = "service",
        //        Code = "genetic code",
        //        PartnerName = "random",
        //        BeginDate = DateTime.Now.AddDays(-2),
        //        EndDate = DateTime.Now.AddDays(2),
        //    },
        //    new PromoCode()
        //    {
        //        Id = Guid.Parse("c9bda72e-fc15-4256-a356-4260b3655cbd"),
        //        ServiceInfo = "service2",
        //        Code = "genetic code2",
        //        PartnerName = "random2",
        //        BeginDate = DateTime.Now.AddDays(-3),
        //        EndDate = DateTime.Now.AddDays(3),
        //    },
        //    new PromoCode()
        //    {
        //        Id = Guid.Parse("c1bda72e-fc55-4256-a356-4760b3658cbd"),
        //        ServiceInfo = "service1",
        //        Code = "genetic code1",
        //        PartnerName = "random1",
        //        BeginDate = DateTime.Now.AddDays(-6),
        //        EndDate = DateTime.Now.AddDays(6),
        //    }
        //};

        //public static IEnumerable<Customer> Customers
        //{
        //    get
        //    {
        //        var customerId = Guid.Parse("a6c8c6b1-4349-45b0-ab31-244740aaf0f0");
        //        var customers = new List<Customer>()
        //        {
        //            new Customer()
        //            {
        //                Id = customerId,
        //                Email = "ivan_sergeev@mail.ru",
        //                FirstName = "Иван",
        //                LastName = "Петров",
        //                CustomerPreferences = new List<CustomerPreference>()
        //                {
        //                    new CustomerPreference()
        //                    {
        //                        CustomerId = customerId,
        //                        PreferenceId = Guid.Parse("76324c47-68d2-472d-abb8-33cfa8cc0c84")
        //                    }
        //                },
        //                PromoCode = new PromoCode()
        //                {
        //                    Id = Guid.Parse("c1bda72e-fc55-4256-a356-4760b5367cbd"),
        //                    ServiceInfo = "service12",
        //                    Code = "genetic code12",
        //                    PartnerName = "random12",
        //                    BeginDate = DateTime.Now.AddDays(-63),
        //                    EndDate = DateTime.Now.AddDays(63),
        //                }
        //            },
        //        };

        //        return customers;
        //    }
        //}
    }
}