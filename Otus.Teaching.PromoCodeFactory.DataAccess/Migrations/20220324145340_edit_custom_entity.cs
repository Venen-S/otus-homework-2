﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Migrations
{
    public partial class edit_custom_entity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Customers_PromoCodes_PromoCodeId",
                table: "Customers");

            migrationBuilder.AddForeignKey(
                name: "FK_Customers_PromoCodes_PromoCodeId",
                table: "Customers",
                column: "PromoCodeId",
                principalTable: "PromoCodes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Customers_PromoCodes_PromoCodeId",
                table: "Customers");

            migrationBuilder.AddForeignKey(
                name: "FK_Customers_PromoCodes_PromoCodeId",
                table: "Customers",
                column: "PromoCodeId",
                principalTable: "PromoCodes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
